#!/usr/bin/env python
# coding: utf-8

# In[4]:


import requests
from bs4 import BeautifulSoup
import csv


#Request webpage content
result = requests.get('https://www.valuewalk.com/2019/01/top-10-most-obese-countries-oecd-who/')

#Save content in var
src = result.content

#soupactivate
soup = BeautifulSoup(src,'lxml')

#look for table
tbl = soup.findAll('ol')
tbl2 = tbl[1]

#Open CSV
file = open('obesecountries.csv','w')
writer = csv.writer(file)

for li in tbl2.findAll('li'):
    rowtext = li.get_text()
    writer.writerow([rowtext])
    
file.close()


# In[ ]:




