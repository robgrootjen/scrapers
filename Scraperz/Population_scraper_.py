#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import requests
from bs4 import BeautifulSoup
import csv


#Request webpage content
result = requests.get('https://www.worldometers.info/world-population/population-by-country/')

#Save content in var
src = result.content

#soupactivate
soup = BeautifulSoup(src,'lxml')

#save countrytable in csv
tbl = soup.find('table')
with open('worldpopulation.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    for tr in tbl('tr'):
        row = [t.get_text(strip=True) for t in tr(['td', 'th'])]
        writer.writerow(row)


# In[56]:





# In[ ]:




